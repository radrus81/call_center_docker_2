#!/bin/bash
set -e

if [ "$1" = 'node' ]; then

	cd /var/www

    if ! ls -1qA node_modules | grep -q .; then
		echo "installing dependencies"
		npm install
	fi

	exec docker-entrypoint.sh npm run prod
fi

exec "$@"
