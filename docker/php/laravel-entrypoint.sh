#!/bin/bash
set -e

if [ "$1" = 'laravel' ]; then

	cd /var/www

	if [ ! -d "/var/www/vendor" ]; then
		echo "installing dependencies"
		composer install
	fi

	if [ ! -f /var/www/.env ]; then
	    echo "initializing app"
		cp .env.example .env
		php artisan key:generate
		php artisan storage:link
		#php artisan db:seed
    fi

	exec docker-php-entrypoint php-fpm
fi

exec "$@"
