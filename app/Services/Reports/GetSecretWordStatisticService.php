<?php

namespace App\Services\Reports;

use Illuminate\Support\Facades\DB;
use App\Services\HelpersService;
use App\Services\DirectoriesService;


class GetSecretWordStatisticService
{

    public function __construct(DirectoriesService $directoriesService, HelpersService $delpersService)
    {
        $this->directoriesService = $directoriesService;
        $this->helpersService = $delpersService;
    }


    public function getSecretWordStatistic($request)
    {
        $dateTimeFrom = $this->helpersService->getDataTimeShamp($request->dateFrom, 'from');
        $dateTimeTo = $this->helpersService->getDataTimeShamp($request->dateTo, 'to');
        $codesUpfr = $this->directoriesService->getCodesUpfr($request->access, $request->codeUpfr);

        $rawData = DB::table('upfr')
            ->select(DB::raw('upfr.name_upfr,statussecretword.name,count(calls.id) as count'))
            ->leftjoin('calls', function ($join) use ($dateTimeFrom, $dateTimeTo) {
                $join->on('calls.kod_upfr', 'upfr.id_upfr')
                    ->where(function ($query) use ($dateTimeFrom, $dateTimeTo) {
                        $query->whereBetween('calls.time_start', [$dateTimeFrom, $dateTimeTo]);
                    });
                $join->on('calls.kod_upfr', 'upfr.id_upfr');
            })
            ->leftJoin('statussecretword', 'statussecretword.idSecretWord', '=', 'calls.kod_otvet')
            ->whereIn('upfr.id_upfr', $codesUpfr)
            ->groupBy('calls.kod_otvet', 'upfr.name_upfr', 'statussecretword.name')
            ->get();
        return $this->convertRawDataForExcel($rawData, $codesUpfr);
    }

    private function convertRawDataForExcel($rawData, $codesUpfr)
    {
        $namesUpfr = $this->getNamesUpfr($codesUpfr);
        $haveData = array();
        foreach ($rawData as $index => $value) {
            $haveData[$value->name_upfr][$value->name] = $value->count;
        }
        $readyDataForExcel = array();
        foreach ($namesUpfr as $index => $upfr) {
            $dataOnlyForUpfr = array();
            $dataOnlyForUpfr['nameUpfr'] = $upfr->name_upfr;
            if (array_key_exists($upfr->name_upfr, $haveData)) {
                $dataOnlyForUpfr = $this->isKeyFromArray('Не спрашивалось', $haveData, $upfr->name_upfr, $dataOnlyForUpfr);
                $dataOnlyForUpfr = $this->isKeyFromArray('Ответил правильно', $haveData, $upfr->name_upfr, $dataOnlyForUpfr);
                $dataOnlyForUpfr = $this->isKeyFromArray('Ответил неправильно', $haveData, $upfr->name_upfr, $dataOnlyForUpfr);
            } else {
                $dataOnlyForUpfr['Не спрашивалось'] = 0;
                $dataOnlyForUpfr['Ответил правильно'] = 0;
                $dataOnlyForUpfr['Ответил неправильно'] = 0;
            }
            $readyDataForExcel[] = $dataOnlyForUpfr;
        }
        return $readyDataForExcel;
    }

    private function isKeyFromArray($keyName, $array, $nameUpfr, $dataOnlyForUpfr)
    {
        if (array_key_exists($keyName, $array[$nameUpfr])) {
            $dataOnlyForUpfr[$keyName] = $array[$nameUpfr][$keyName];
        } else {
            $dataOnlyForUpfr[$keyName] = 0;
        }
        return $dataOnlyForUpfr;
    }

    private function getNamesUpfr($codesUpfr)
    {
        return DB::table('upfr')
            ->select(DB::raw('name_upfr'))
            ->whereIn('id_upfr', $codesUpfr)
            ->get();
    }
}
