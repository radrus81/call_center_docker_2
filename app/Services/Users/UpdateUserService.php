<?php

namespace App\Services\Users;


use Illuminate\Support\Facades\DB;

class UpdateUserService
{
    public function updateUser($request)
    {
        $data = $request->only('id', 'login', 'pass', 'kod_upfr', 'fio', 'access', 'isBlocked');
        $update = array();
        foreach ($data as $key => $value) {
            switch ($key) {
                case 'id':
                case '_method':
                    break;
                case 'pass':
                    if (trim($value) !== '' && $value !== 'undefined') {
                        $update += [$key => md5($value)];
                    }
                    break;
                case 'access':
                    $update += ['dostup' => $value];
                    break;

                default:
                    $update += [$key => $value];
            }
        }
        DB::table('users')
            ->where('id', $data['id'])
            ->update($update);

        return $this->getDataUser($data['id']);
    }



    public function getDataUser($id)
    {
        return DB::table('users')
            ->select(
                'users.id',
                'users.login',
                'upfr.name_upfr',
                'dostup.name_dostup',
                'dostup.id_dostup as id_dostup',
                'users.fio as userFullName',
                'users.kod_upfr',
                'users.isBlocked'
            )
            ->leftJoin('upfr', 'upfr.id_upfr', '=', 'users.kod_upfr')
            ->leftJoin('dostup', 'dostup.id_dostup', '=', 'users.dostup')
            ->where('users.id', $id)
            ->first();
    }
}
