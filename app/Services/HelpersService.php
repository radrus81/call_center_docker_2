<?php

namespace App\Services;

use Carbon\Carbon;

class HelpersService
{
    public function getDataTimeShamp($dateTime, $typeDate)
    {
        $dateArr = explode('-', $dateTime);

        if ($typeDate == 'from') {
            $hour = 0;
            $minute = 0;
            $second = 0;
        } else {
            $hour = 23;
            $minute = 59;
            $second = 59;
        }
        $year = $dateArr[0];
        $month = $dateArr[1];
        $day =  $dateArr[2];
        $newDate =  Carbon::create($year, $month, $day, $hour, $minute, $second, 'UTC');
        return Carbon::parse($newDate)->timestamp;
    }

    public function getPartsFromFullName($fullName)
    {
        $arrayPartsFullName = array();
        $name = '';
        $father = '';
        $partsFullName = explode(" ", $fullName);
        foreach ($partsFullName as $value) {
            if (!empty($value)) {
                array_push($arrayPartsFullName, $value);
            }
        }
        $count = count($arrayPartsFullName);
        $family = $arrayPartsFullName[0];
        if ($count >= 2) {
            $name = $arrayPartsFullName[1];
        }
        if ($count >= 3) {
            $father = $arrayPartsFullName[2];
        }
        return array('family' => $family, 'name' => $name, 'father' => $father);
    }
}
