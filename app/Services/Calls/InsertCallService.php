<?php

namespace App\Services\Calls;

use Illuminate\Support\Facades\DB;
use App\Services\HelpersService;


class InsertCallService
{
    public function __construct(HelpersService $helpersService)
    {
        $this->helpersService = $helpersService;
    }

    public function InsertCall($request)
    {
        for ($countRecord = 0; $countRecord < $request->countRecord; $countRecord++) {
            DB::table('calls')->insert($this->getInsertData($request));
        }
    }


    private function getInsertData($request)
    {
        if (empty($request->fullNamePensioner)) {
            $partsFullName = array('family' => 'Не представился', 'name' => 'Не представился', 'father' => 'Не представился');
        } else {
            $partsFullName = $this->helpersService->getPartsFromFullName($request->fullNamePensioner);
        }

        $insert = array(
            array(
                'time_start' => $request->timeStart, 'time_ok' => $request->timeEnd, 'time_treb_otvet' => $request->timeEnd,
                'time_perekl' => $request->timeEnd, 'time_zapis_priem' => $request->timeEnd, 'time_zakaz_doc' => $request->timeEnd,
                'fam' => $partsFullName['family'], 'name' => $partsFullName['name'], 'otch' => $partsFullName['father'], 'snils' => $request->snils,
                'kod_otvet' => $request->codeWord, 'phone' => $request->phone, 'raion' => $request->areaOfDislocation,
                'type' => $request->typeCounterparty, 'topic' => $request->topic, 'subtopic' => $request->subTopic,
                'typeconsult' => $request->typeConsultation, 'resultconsult' => $request->resultConsultation, 'prim' => $request->notice,
                'user' => $request->idUser, 'codeuser' => $request->codeUser, 'kod_upfr' => $request->codeUpfr,
                'status' => $request->status
            )
        );
        if ($request->isСomplaint === 'true') {
            $insert[0] += ['complaintСodeUpfr' =>  $request->complaintsToUpfr];
            $insert[0] += ['dateOfIssue' =>  $request->dateOfIssue];
            $insert[0] += ['complaintDescription' =>  $request->complaintsDescription];
        }
        return $insert;
    }
}
