<?php

namespace App\Services\Calls;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use App\Services\DirectoriesService;
use App\Services\HelpersService;

class GetCallsService
{

    public function __construct(DirectoriesService $directoriesService, HelpersService $helpersService)
    {
        $this->directoriesService = $directoriesService;
        $this->helpersService = $helpersService;
    }


    public function getDataCalls($request)
    {
        $codesUpfr = $this->directoriesService->getCodesUpfr($request->access, $request->codeUpfr);
        $pageSize = $request->pageSize;

        return DB::table('calls')
            ->select(
                DB::raw("calls.id as idCall,calls.kod_upfr as codeUpfr,
            DATE_FORMAT(FROM_UNIXTIME(calls.time_start), '%d.%m.%Y %H:%i:%s') as dateStart,
            calls.fam as familyClient,
            calls.name as nameClient,
            calls.otch as middleNameClient,
            COALESCE(calls.snils,'') as snils,
            COALESCE(calls.phone,'') as phoneClient,
            raion.nazv as areaOfDislocation,
            kontragent.name as typeOfCounterparty,
            COALESCE(tema.name,'') as nameTopicOld,
            COALESCE(topics.topic,'') as topic,
            COALESCE(subtopics.subtopic,'') as subtopic,
            COALESCE(typeconsultation.type,'')  as typeConsultation,
            COALESCE(resultconsultation.result,'')  as resultConsultation,
            COALESCE(calls.prim,'') as notice,
            users.fio as nameUser,
            if (calls.kod_otvet=1, 'да' , if (calls.kod_otvet=0,'','нет')) as secretWord,
            COALESCE(upfr.name_upfr,'нет жалобы') as complaintСodeUpfr,
            if (DATE_FORMAT(calls.dateOfIssue, '%d.%m.%Y') = '00.00.0000','',DATE_FORMAT(calls.dateOfIssue, '%d.%m.%Y')) as dateOfIssue,
            COALESCE(calls.complaintDescription,'') as complaintDescription,
            status.id_status as statusName,
            calls.status")
            )
            ->leftjoin('raion', 'raion.id', '=', 'calls.raion')
            ->leftjoin('tema', 'tema.id', '=', 'calls.tema')
            ->leftjoin('status', 'status.id_status', '=', 'calls.status')
            ->leftjoin('topics', 'topics.id', '=', 'calls.topic')
            ->leftjoin('subtopics', 'subtopics.id', '=', 'calls.subtopic')
            ->leftjoin('typeconsultation', 'typeconsultation.id', '=', 'calls.typeconsult')
            ->leftjoin('resultconsultation', 'resultconsultation.id', '=', 'calls.resultconsult')
            ->leftjoin('kontragent', 'kontragent.id', '=', 'calls.type')
            ->leftjoin('users', 'users.id', '=', 'calls.user')
            ->leftjoin('upfr', 'calls.complaintСodeUpfr', '=', 'upfr.id_upfr')
            ->whereIn('calls.kod_upfr', $codesUpfr)
            ->where($this->getWhere($request))
            ->orderBy('calls.id', 'desc')
            ->offset($this->getOffset($request, $pageSize))
            ->limit($this->getLimit($request, $pageSize))
            ->get();
    }

    public function getTotalCountCalls($request)
    {
        $codesUpfr = $this->directoriesService->getCodesUpfr($request->access, $request->codeUpfr);

        return DB::table('calls')
            ->select('calls.id')
            ->whereIn('calls.kod_upfr', $codesUpfr)
            ->where($this->getWhere($request))
            ->count();
    }

    public function getTotalCountCallsForCurrentDate($request)
    {
        $codesUpfr = $this->directoriesService->getCodesUpfr($request->access, $request->codeUpfr);
        return DB::table('calls')
            ->select('id')
            ->whereBetween('time_start', [$this->converDateToTimestamp('since'), $this->converDateToTimestamp('to')])
            ->whereIn('calls.kod_upfr', $codesUpfr)
            ->count();
    }

    public function getUserStatistic($idUser, $access)
    {
        $currentDateS = $this->converDateToTimestamp('since');
        $currentDateTo = $this->converDateToTimestamp('to');
        $accessAdmin = config('enums.ACCESS_ADMIN');
        $accessControllerOpfr = config('enums.ACCESS_USER_CONTROLLER_OPFR', 6);
        $where = array();
        if (!in_array((int) $access, [$accessAdmin, $accessControllerOpfr])) {
            $where[] = ['calls.user', $idUser];
        } else {
            $where[] = ['calls.user', '>', 0];
        }
        return  DB::table('status')
            ->select(DB::raw('status.id_status,count(calls.id) as count'))
            ->leftjoin('calls', function ($join) use ($currentDateS, $currentDateTo, $where, $idUser) {
                $join->on('calls.status', '=', 'status.id_status')
                    ->where(function ($query) use ($currentDateS, $currentDateTo, $where, $idUser) {
                        $query->whereBetween('calls.time_start', [$currentDateS, $currentDateTo]);
                        $query->where($where);
                    });
            })
            ->groupBy('id_status')
            ->get();
    }


    public function getUserLastCalls($idUser)
    {
        return DB::table('calls')
            ->select(DB::raw('calls.id as idCall,
            DATE_FORMAT(FROM_UNIXTIME(calls.time_start), "%d.%m.%Y %H:%i:%s") as dateStart,
            CONCAT(calls.fam," ",calls.name," ",calls.otch) as fullNamePensioner,
            COALESCE(subtopics.subtopic,"") as subtopic,
            calls.status'))
            ->leftjoin('subtopics', 'subtopics.id', '=', 'calls.subtopic')
            ->where('calls.user', $idUser)
            ->orderBy('calls.id', 'desc')
            ->offset(0)
            ->limit(5)
            ->get();
    }

    private function getOffset($request, $pageSize)
    {
        if ($request->activePage) {
            return ($request->activePage * $pageSize) - $pageSize;
        } else {
            return 0;
        }
    }

    private function getLimit($request, $pageSize)
    {
        if ($request->activePage) {
            return $pageSize;
        } else {
            return $this->getTotalCountCalls($request);
        }
    }

    private function getWhere($request)
    {
        $where = array();
        if ($request->isFilter === 'true') {
            if (!is_null($request->codeUpfrFromFilter) && $request->codeUpfrFromFilter) {
                $where[] = ['calls.kod_upfr', $request->codeUpfrFromFilter];
            }
            if (($request->createDateFromFilter != 'null') && $request->createDateFromFilter) {
                $dateTimeFrom = $this->helpersService->getDataTimeShamp($request->createDateFromFilter, 'from');
                $dateTimeTo = $this->helpersService->getDataTimeShamp($request->createDateFromFilter, 'to');
                $where[] = ['calls.time_start', '>', $dateTimeFrom];
                $where[] = ['calls.time_start', '<', $dateTimeTo];
            }
            if (!is_null($request->fullNameClientFromFilter) && $request->fullNameClientFromFilter) {
                $where[] = ['calls.fam', 'like', '%' . $request->fullNameClientFromFilter . '%'];
            }
            if (!is_null($request->phone) && $request->phone) {
                $where[] = ['calls.phone', 'like', '%' . $request->phone . '%'];
            }
            if (!is_null($request->typeContractorFromFilter) && $request->typeContractorFromFilter) {
                $where[] = ['calls.type', $request->typeContractorFromFilter];
            }
            if (!is_null($request->subTopicFromFilter) && $request->subTopicFromFilter) {
                $where[] = ['calls.subtopic', $request->subTopicFromFilter];
            }
            if (!is_null($request->userFromFilter) && $request->userFromFilter) {
                $where[] = ['calls.user', $request->userFromFilter];
            }
            if (!is_null($request->noticeFromFilter) && $request->noticeFromFilter) {
                $where[] = ['calls.prim', 'like', '%' . $request->noticeFromFilter . '%'];
            }
            if (
                !is_null($request->statusFromFilter) && $request->statusFromFilter != 'NaN'
                && $request->statusFromFilter != 'undefined'
            ) {
                $where[] = ['calls.status', $request->statusFromFilter];
            }
        }
        if ($request->typeReport === 'printJournal') {
            $dateFromArr = explode('-', $request->dateFrom);
            $dateFromTo = explode('-', $request->dateTo);
            $newDateFrom =  Carbon::create($dateFromArr[0], $dateFromArr[1], $dateFromArr[2], 0, 0, 0, 'UTC');
            $newDateTo =  Carbon::create($dateFromTo[0], $dateFromTo[1], $dateFromTo[2], 23, 59, 59, 'UTC');
            $where[] = ['calls.time_start', '>', Carbon::parse($newDateFrom)->timestamp];
            $where[] = ['calls.time_start', '<', Carbon::parse($newDateTo)->timestamp];
        }
        if (!count($where)) {
            $where[] = [`1`, '=', `1`];
        }
        return $where;
    }

    private function converDateToTimestamp($labelDateTime)
    {

        $currentDate = Carbon::now();

        if ($labelDateTime == 'since') {
            $hour = 0;
            $minute = 0;
            $second = 0;
        } else {
            $hour = 23;
            $minute = 59;
            $second = 59;
        }
        $year = $currentDate->format('Y');
        $month = $currentDate->format('m');
        $day =  $currentDate->format('d');
        $newDate =  Carbon::create($year, $month, $day, $hour, $minute, $second, 'UTC');
        return Carbon::parse($newDate)->timestamp;
    }
}
