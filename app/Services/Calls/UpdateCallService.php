<?php

namespace App\Services\Calls;

use Carbon\Carbon;
use Illuminate\Support\Facades\DB;


class UpdateCallService
{

    public function UpdateCall($request)
    {
        $expectedFields = [
            'familyClient' => 'fam', 'nameClient' => 'name', 'middleNameClient' => 'otch', 'snils' => 'snils', 'phoneClient' => 'phone',
            'areaOfDislocation' => 'raion', 'typeOfCounterparty' => 'type', 'nameTopicOld' => 'tema', 'topic' => 'topic', 'subtopic' => 'subtopic',
            'typeConsultation' => 'typeconsult', 'resultConsultation' => 'resultconsult', 'notice' => 'prim', 'complaintСodeUpfr' => 'complaintСodeUpfr',
            'dateOfIssue' => 'dateOfIssue', 'complaintDescription' => 'complaintDescription'
        ];
        $update = array();
        foreach ($expectedFields as $field => $fieldInBase) {
            if ($request->$field) {
                $codeField = $this->getIdField($fieldInBase, $request->$field);
                $update += [$fieldInBase => $codeField];
            }
        }
        return DB::table('calls')
            ->where('id', $request->idCall)
            ->update($update);
    }

    private function getIdField($fieldInBase, $valueFromUser)
    {

        switch ($fieldInBase) {
            case 'raion':
                return DB::table('raion')->where('nazv', $valueFromUser)->pluck('id')[0];
                break;
            case 'type':
                return DB::table('kontragent')->where('name', $valueFromUser)->pluck('id')[0];
                break;
            case 'topic':
                return DB::table('topics')->where('topic', $valueFromUser)->pluck('id')[0];
                break;
            case 'subtopic':
                return DB::table('subtopics')->where('subtopic', $valueFromUser)->pluck('id')[0];
                break;
            case 'typeconsult':
                return DB::table('typeconsultation')->where('type', $valueFromUser)->pluck('id')[0];
                break;
            case 'resultconsult':
                return DB::table('resultconsultation')->where('result', $valueFromUser)->pluck('id')[0];
                break;
            case 'complaintСodeUpfr':
                return DB::table('upfr')->where('name_upfr', $valueFromUser)->pluck('id_upfr')[0];
                break;
            case 'dateOfIssue':
                return Carbon::parse($valueFromUser)->format('Y-m-d');
                break;
            default:
                return $valueFromUser;
        }
    }
}
