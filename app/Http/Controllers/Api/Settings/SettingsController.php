<?php

namespace App\Http\Controllers\Api\Settings;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class SettingsController extends Controller
{

    public function getSettings()
    {
        // die(env('DB_USERNAME'));

        $settingsApp = DB::table('settingapp')
            ->select("name", "value")
            ->get();

        return response()->json([
            'settingsApp' => $settingsApp
        ]);
    }
}
