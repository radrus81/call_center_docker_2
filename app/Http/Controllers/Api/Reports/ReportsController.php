<?php

namespace App\Http\Controllers\Api\Reports;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\Reports\ReportsService;


class ReportsController extends Controller
{

    public function __construct(ReportsService $reportsService)
    {
        $this->reportsService = $reportsService;
    }

    public function createReport(Request $request)
    {
        return response()->json([
            'dataForExcel' => $this->reportsService->getDataForExcel($request)
        ]);
    }
}
