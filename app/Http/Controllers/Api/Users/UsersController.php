<?php

namespace App\Http\Controllers\Api\Users;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Services\Users\AddUserService;
use App\Services\Users\GetUpfrAndAccessService;
use App\Services\Users\UpdateUserService;
use App\Services\Users\GetDataUsersService;

class UsersController extends Controller
{

    public function __construct(
        AddUserService $addUserService,
        GetUpfrAndAccessService $getUpfrAndAccessService,
        UpdateUserService $updateUserService,
        GetDataUsersService $getDataUsersService

    ) {
        $this->addUserService = $addUserService;
        $this->getUpfrAndAccessService = $getUpfrAndAccessService;
        $this->updateUser = $updateUserService;
        $this->getDataUsersService = $getDataUsersService;
    }

    public function getUpfrList()
    {
        return response()->json([
            'upfrList' => $this->getUpfrAndAccessService->getUpfrList(),
            'listAccess' => $this->getUpfrAndAccessService->getAccessList(),
            'successMessage' => ''
        ], 200);
    }

    public function addUser(Request $request)
    {
        $resultAdd = array();
        $resultAdd = $this->addUserService->addUser($request);
        return response()->json([
            'newDataUser' => $resultAdd['dataUser']
        ],  $resultAdd['resultCode']);
    }

    public function getDataFromTableUsers(Request $request)
    {
        return response()->json([
            'usersList' => $this->getDataUsersService->getDataFromTableUsers($request),
            'successMessage' => ''
        ], 200);
    }

    public function updateUser(Request $request)
    {
        return response()->json([
            'newDataUser' => $this->updateUser->updateUser($request),
            'successMessage' => 'Пользователь успешно отредактирован'
        ], 200);
    }
}
