<?php

namespace App\Http\Controllers\Api\Calls;

use App\Http\Controllers\Controller;
use App\Services\SecretWord\SecretWordService;

class SecretWordController extends Controller
{

    public function __construct(SecretWordService $secretWordService)
    {
        $this->secretWordService = $secretWordService;
    }

    public function getSecretWord($snils)
    {
        return response()->json([
            'dataSecretWord' => $this->secretWordService->getQuestionAndAnswer($snils)
        ], 200);
    }
}
