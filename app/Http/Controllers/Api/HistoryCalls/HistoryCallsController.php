<?php

namespace App\Http\Controllers\Api\HistoryCalls;

use App\Http\Controllers\Controller;
use App\Services\HistoryCalls\GetHistoryCallsService;


class HistoryCallsController extends Controller
{

    public function __construct(GetHistoryCallsService $getHistoryCallsService)
    {
        $this->getHistoryCallsService = $getHistoryCallsService;
    }

    public function getHistoryCalls($page, $fullNamePensioner)
    {
        $dataHistoryCalls = $this->getHistoryCallsService->getHistoryCalls($page, $fullNamePensioner);
        return response()->json([
            'dataHistoryCalls' => $dataHistoryCalls['dataCalls'],
            'totalCount' => $dataHistoryCalls['totalCount']
        ]);
    }
}
