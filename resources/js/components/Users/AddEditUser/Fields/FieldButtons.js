import React from 'react'
import { SubmitBtn } from 'react-formik-ui'


export const FieldButtons = ({ isSubmitting, isInEditMode, cancelEditUser }) => (

    <div className="btnBlockEdit">
        <SubmitBtn id="send" className="btn-primary"
            type="submit" disabled={isSubmitting}>
            {isInEditMode ? 'Сохранить' : 'Добавить'}
        </SubmitBtn>
        {isInEditMode ?
            <button id="cancelEdit" className="btn-primary"
                type="button" onClick={() => { cancelEditUser() }}>
                Отменить
            </button>
            : null
        }
    </div>

)
