import React from 'react'
import { Field } from 'formik'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faKey } from '@fortawesome/free-solid-svg-icons'


export const FieldPassword = ({ errorPass, showFieldPasswordEdit, valuePass, touchedPass, isInEditMode }) => {

    return (
        <>
            {
                errorPass && touchedPass && !isInEditMode &&
                <div className="messErr alert-danger">{errorPass}</div>
            }
            {
                showFieldPasswordEdit === 'enabled' ?
                    < div className="iconInput">
                        <FontAwesomeIcon icon={faKey} size="lg" />
                        <Field
                            name="pass"
                            placeholder="Введите пароль"
                            type="password"
                            value={valuePass || ''}
                        />
                    </div>
                    : <Field type="hidden" name="pass" value="12345678" />
            }
        </>
    )
}
