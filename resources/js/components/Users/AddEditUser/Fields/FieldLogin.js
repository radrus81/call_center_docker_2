import React from 'react'
import { Field } from 'formik'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faIdCard } from '@fortawesome/free-solid-svg-icons'


export const FieldLogin = ({ errorLogin, valueLogin, touchedLogin }) => (
    <>
        {
            errorLogin && touchedLogin &&
            <div className="messErr alert-danger">{errorLogin}</div>
        }
        <div className="iconInput">
            <FontAwesomeIcon icon={faIdCard} size="lg" />
            <Field
                name="login"
                placeholder="Введите логин"
                type="text"
                value={valueLogin || ''}
            />
        </div>
    </>
)
