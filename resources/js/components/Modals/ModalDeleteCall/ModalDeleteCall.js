import React from 'react'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'

export const ModalDeleteCall = ({ isModalOpen, isModalClose, dataCall, deleteCall, modalClose }) => {

    return (
        < Modal size="lg" isOpen={isModalOpen} unmountOnClose={isModalClose}>
            <ModalHeader>Подтвердите удаление<br /></ModalHeader>
            <ModalBody>
                <div className="alert alert-warning" role="alert">
                    Вы действительно хотите удалить запись {dataCall.familyClient + ' ' + dataCall.nameClient + ' ' + dataCall.middleNameClient}?
                </div>
            </ModalBody>
            <ModalFooter>
                <Button color="danger" onClick={deleteCall}>Удалить</Button>
                <Button color="secondary" onClick={modalClose}>Отмена</Button>
            </ModalFooter>
        </Modal >
    )
}
