import React from 'react'
import { Tablesrow } from './Tablesrow'


export const TableHistoryCalls = ({ dataHistoryCalls, copyCall }) => {

    return (
        <table className="table">
            <thead className="thead-dark">
                <tr>
                    <th width="6%"></th>
                    <th width="10%">Дата</th>
                    <th width="24%">ФИО</th>
                    <th width="10%">СНИЛС</th>
                    <th width="10%">Телефон</th>
                    <th width="10%">Район проживания</th>
                    <th width="20%">Причина обращения</th>
                    <th width="20%">Примечание</th>
                    <th width="9%">Специалист</th>
                    <th width="11%">Статус</th>
                </tr >
            </thead>
            <tbody>
                <Tablesrow
                    calls={dataHistoryCalls}
                    copyCall={copyCall}
                />
            </tbody>
        </table>
    )
}
