import React from 'react'
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap'
import { TableHistoryCalls } from './TableHistoryCalls'
import './ModalHistoryCalls.scss'
import { Pagination } from '../../Journal/Pagination'

const PAGE_SIZE = 10

export const ModalHistoryCalls = ({ isModalOpen, isModalClose, dataHistoryCalls,
    totalCount, activePage, errors, modalClose, copyCall, PageChangeHandler }) => {


    return (

        < Modal className="modalHistoryCalls" size="lg" isOpen={isModalOpen} unmountOnClose={isModalClose}>
            <ModalHeader>История звонков<br /></ModalHeader>
            <ModalBody>
                {
                    errors ?
                        <div className="alert alert-danger" role="alert">
                            {errors}
                        </div>
                        :
                        <>
                            {

                                totalCount > PAGE_SIZE ?
                                    <div className="pagintioninfo">
                                        <Pagination
                                            pageSize={PAGE_SIZE}
                                            activePage={activePage}
                                            totalCountCalls={totalCount}
                                            PageChangeHandler={PageChangeHandler}

                                        />  <p> Количество записей всего&nbsp;<span>{totalCount}</span></p>
                                    </div>
                                    : <p> Количество записей всего&nbsp;<span>{totalCount}</span></p>


                            }
                            <TableHistoryCalls
                                dataHistoryCalls={dataHistoryCalls}
                                copyCall={copyCall} />
                        </>
                }
            </ModalBody>
            <ModalFooter>
                <Button color="danger" onClick={modalClose}>Закрыть</Button>
            </ModalFooter>
        </Modal >
    )
}
