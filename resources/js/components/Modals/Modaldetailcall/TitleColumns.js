const dataForTitle = {

    'num': {
        'header': '№ п/п',
        'width': 6,
        'isEdit': false
    },
    'codeUpfr': {
        'header': 'СП',
        'width': 6,
        'isEdit': false
    },
    'nameUpfr': {
        'header': 'Наименование СП',
        'width': 35,
        'isEdit': false,
        'style': {
            alignment: { horizontal: "left", wrapText: true },
            font: { color: { argb: "000000" } }
        }
    },
    'dateStart': {
        'header': 'Дата звонка',
        'width': 22,
        'isEdit': false
    },
    'fioClient': {
        'header': 'ФИО пенсионера',
        'width': 30,
        'style': {
            alignment: { horizontal: "left", wrapText: true },
            font: { color: { argb: "000000" } }
        },
        'isEdit': true
    },
    'familyClient': {
        'header': 'Фaмилия',
        'width': 25,
        'style': {
            alignment: { horizontal: "center", wrapText: true },
            font: { color: { argb: "000000" } }
        },
        'isEdit': true
    },
    'nameClient': {
        'header': 'Имя',
        'width': 25,
        'style': {
            alignment: { horizontal: "center", wrapText: true },
            font: { color: { argb: "000000" } }
        },
        'isEdit': true
    },
    'middleNameClient': {
        'header': 'Отчество',
        'width': 25,
        'style': {
            alignment: { horizontal: "center", wrapText: true },
            font: { color: { argb: "000000" } }
        },
        'isEdit': true
    },
    'typeOfCounterparty': {
        'header': 'Тип контрагента',
        'width': 8,
        'isEdit': true
    },

    'snils': {
        'header': 'СНИЛС',
        'width': 15,
        'isEdit': true
    },
    'phoneClient': {
        'header': 'Телефон',
        'width': 17,
        'isEdit': true
    },
    'areaOfDislocation': {
        'header': 'Район проживания',
        'width': 11,
        'isEdit': true
    },
    'nameTopicOld': {
        'header': 'Старая тема',
        'width': 15,
        'isEdit': false
    },
    'topic': {
        'header': 'Новая тема',
        'width': 15,
        'isEdit': true,
    },
    'topicNewReport': {
        'header': 'Тема обращения',
        'width': 20,
        'isEdit': true,
    },
    'topicStat': {
        'header': 'Тема обращения',
        'width': 68,
        'isEdit': true,
        'style': {
            alignment: { horizontal: "left", wrapText: true },
            font: { color: { argb: "000000" } }
        },
    },
    'subtopic': {
        'header': 'Подтема обращения',
        'width': 30,
        'isEdit': true
    },
    'typeConsultation': {
        'header': 'Вид консультации',
        'width': 60,
        'isEdit': true
    },
    'resultConsultation': {
        'header': 'Результат консультации',
        'width': 60,
        'isEdit': true
    },
    'notice': {
        'header': 'Примечание',
        'width': 15,
        'isEdit': true
    },
    'durationCall': {
        'header': 'Продолжительность разговора',
        'width': 15,
        'isEdit': false
    },
    'nameUser': {
        'header': 'ФИО оператора',
        'width': 30,
        'isEdit': false
    }, 'codeUser': {
        'header': 'Код оператора',
        'width': 10,
        'isEdit': false
    },
    'secretWord': {
        'header': 'Секретное слово',
        'width': 12,
        'isEdit': false
    },
    'dateOfIssue': {
        'header': 'Дата обращения',
        'width': 12,
        'isEdit': true
    },
    'complaintDescription': {
        'header': 'Описание ситуации',
        'width': 12,
        'isEdit': true
    },
    'complaintСodeUpfr': {
        'header': 'Жалоба на УПФР',
        'width': 12,
        'isEdit': true
    },
    'count': {
        'header': 'Кол-во звонков',
        'width': 12,
        'isEdit': false
    },
    'opfr_83': {
        'header': 'ОПФР',
        'width': 12,
        'isEdit': false
    }, 'upfr_1': {
        'header': 'Барыш',
        'width': 12,
        'isEdit': false
    }, 'upfr_2': {
        'header': 'Димит.',
        'width': 12,
        'isEdit': false
    }, 'upfr_3': {
        'header': 'Базарносыз.',
        'width': 12,
        'isEdit': false
    }, 'upfr_5': {
        'header': 'Вешкайма',
        'width': 12,
        'isEdit': false
    }, 'upfr_6': {
        'header': 'Инза',
        'width': 12,
        'isEdit': false
    }, 'upfr_7': {
        'header': 'Карсун',
        'width': 12,
        'isEdit': false
    }, 'upfr_8': {
        'header': 'Кузоват.',
        'width': 12,
        'isEdit': false
    }, 'upfr_9': {
        'header': 'Майна',
        'width': 12,
        'isEdit': false
    }, 'upfr_11': {
        'header': 'Николаевка',
        'width': 12,
        'isEdit': false
    }, 'upfr_12': {
        'header': 'Нов.малыкла',
        'width': 12,
        'isEdit': false
    }, 'upfr_13': {
        'header': 'Новоспасск',
        'width': 12,
        'isEdit': false
    }, 'upfr_14': {
        'header': 'Павловка',
        'width': 12,
        'isEdit': false
    }, 'upfr_15': {
        'header': 'Радищево',
        'width': 12,
        'isEdit': false
    }, 'upfr_16': {
        'header': 'Сенгилей',
        'width': 12,
        'isEdit': false
    }, 'upfr_17': {
        'header': 'Кулатка',
        'width': 12,
        'isEdit': false
    }, 'upfr_18': {
        'header': 'Майна',
        'width': 12,
        'isEdit': false
    }, 'upfr_19': {
        'header': 'Сурское',
        'width': 12,
        'isEdit': false
    }, 'upfr_19': {
        'header': 'Сурское',
        'width': 12,
        'isEdit': false
    }, 'upfr_20': {
        'header': 'Тереньга',
        'width': 12,
        'isEdit': false
    }, 'upfr_21': {
        'header': 'Новоульян.',
        'width': 12,
        'isEdit': false
    }, 'upfr_22': {
        'header': 'Цильна',
        'width': 12,
        'isEdit': false
    }, 'upfr_23': {
        'header': 'Чердаклы',
        'width': 12,
        'isEdit': false
    }, 'upfr_24': {
        'header': 'Ленинский',
        'width': 12,
        'isEdit': false
    }, 'upfr_25': {
        'header': 'Заволж.',
        'width': 12,
        'isEdit': false
    }, 'upfr_26': {
        'header': 'Желез.',
        'width': 12,
        'isEdit': false
    }, 'upfr_28': {
        'header': 'Засвияж.',
        'width': 12,
        'isEdit': false
    }

}


export function getTitle(key) {

    var defaultStyleHeader = {
        alignment: { horizontal: "center" },
        font: { color: { argb: "000000" } }
    }
    if (typeof dataForTitle[key] !== 'undefined') {
        dataForTitle[key].key = key
        if (typeof dataForTitle[key]['style'] == 'undefined') { //если нет стиля то добавим стили по умолчанию
            dataForTitle[key].style = defaultStyleHeader
        }
        return dataForTitle[key]
    } else {
        return {
            'key': key,
            'header': key,
            'width': 12,
            'style': defaultStyleHeader
        }
    }
}
