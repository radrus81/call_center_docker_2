import React from "react";
import MaskedInput from "react-text-mask";

const phoneNumberMask = [
    "+",
    "7",
    "(",
    /[0-9]/,
    /\d/,
    /\d/,
    ")",
    " ",
    /\d/,
    /\d/,
    /\d/,
    "-",
    /\d/,
    /\d/,
    /\d/,
    /\d/
];

const Phone = ({ fieldKey, value, lossOfFocusInfield }) => (
    <MaskedInput
        type="text"
        mask={phoneNumberMask}
        autoFocus
        className="form-control"
        placeholder="Введите номер тел."
        defaultValue={value}
        onChange={event => localStorage.setItem(fieldKey, event.target.value)}
        onKeyPress={lossOfFocusInfield}
    ></MaskedInput>
);

export default Phone;
