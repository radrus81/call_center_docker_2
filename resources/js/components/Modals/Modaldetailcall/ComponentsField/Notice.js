import React from 'react'

const Notice = ({ fieldKey, value, lossOfFocusInfield }) => (
    <textarea
        autoFocus
        className="form-control"
        onChange={event =>
            localStorage.setItem(fieldKey, event.target.value)}
        onKeyPress={lossOfFocusInfield}
    >{value}
    </textarea  >
)

export default Notice
