let Components = [];

Components['familyClient'] = require('./FioClient').default
Components['nameClient'] = require('./FioClient').default
Components['middleNameClient'] = require('./FioClient').default
Components['snils'] = require('./Snils').default
Components['phoneClient'] = require('./Phone').default
Components['areaOfDislocation'] = require('../../../../containers/DropDownListField').default
Components['typeOfCounterparty'] = require('../../../../containers/DropDownListField').default
Components['nameTopicOld'] = require('../../../../containers/DropDownListField').default
Components['topic'] = require('../../../../containers/DropDownListField').default
Components['subtopic'] = require('../../../../containers/Subtopic').default
Components['typeConsultation'] = require('../../../../containers/DropDownListField').default
Components['resultConsultation'] = require('../../../../containers/DropDownListField').default
Components['notice'] = require('./Notice').default
Components['complaintСodeUpfr'] = require('../../../../containers/DropDownListField').default
Components['dateOfIssue'] = require('./Date').default
Components['complaintDescription'] = require('./Notice').default


export default Components
