import React from 'react'
import { NavLink } from 'react-router-dom'
import './Header.scss'
import Infopanel from '../../containers/Infopanel'


export const Header = ({ isAuthorization, brand, logout }) => (
    <header className="bg-dark fixed-top">
        <div className="wrap fixed-top">
            {
                isAuthorization ?
                    <>
                        <NavLink to="/">
                            <h1>{brand} </h1>
                        </NavLink>
                        <Infopanel logout={logout} />
                    </> : <h1 className={!isAuthorization ? "band" : null}>
                        {brand}
                    </h1>
            }
        </div>
    </header>
)
