import * as Yup from "yup";

const ReportFormSchema = Yup.object().shape({
    dateFrom: Yup.string()
        .required("Не заполнено поле"),
    dateTo: Yup.string()
        .required("Не заполнено поле")
});

export default ReportFormSchema
