import React from 'react'
import { Label, Col, Alert } from 'reactstrap'
import DatePicker, { registerLocale } from "react-datepicker"
import "react-datepicker/dist/react-datepicker.css"
import ru from "date-fns/locale/ru"; // the locale you want
registerLocale("ru", ru);



export const Period = ({ errors, touched, values, setValueForReport }) => (

    <Col sm={12} className="reportPeriod">
        <Label>Дата звонка С&nbsp;</Label>

        <DatePicker
            name="dateFrom"
            locale="ru"
            selected={values.dateFrom}
            onChange={date => setValueForReport("dateFrom", date)}
            placeholderText="Дата звонка"
            dateFormat="dd.MM.yyyy"
            className={errors.dateFrom && touched.dateFrom ?
                "form-control errorValidation"
                : "form-control"
            }
        />
        <Label>&nbsp;ПО&nbsp;</Label>
        <DatePicker
            name="dateTo"
            locale="ru"
            selected={values.dateTo}
            onChange={date => setValueForReport("dateTo", date)}
            placeholderText="Дата звонка"
            dateFormat="dd.MM.yyyy"
            className={errors.dateTo && touched.dateTo ?
                "form-control errorValidation"
                : "form-control"
            }
        />
    </Col>
)
