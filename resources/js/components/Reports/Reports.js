import React from 'react'
import { Formik } from 'formik'
import { Form, SubmitBtn } from 'react-formik-ui'
import { FormGroup, Col } from 'reactstrap'
import { Period } from './Fields/Period'
import './Report.scss'
import ReportFormSchema from './ReportFormSchema'
import { RadioBtnJournalAndStatisticsTypeTopic } from './Fields/RadioBtnJournalAndStatisticsTypeTopic'
import { RadioBtnStatisticUsers } from './Fields/RadioBtnStatisticUsers'
import { RadioBtnSecretWordStatisticsAndReportPhone } from './Fields/RadioBtnSecretWordStatisticsAndReportPhone'
import { RadioBtnСomplaint } from './Fields/RadioBtnСomplaint'
import ModalServerError from '../../containers/ModalServerError'
import Loader from '../../components/Loader/Loader'
import { ACCESS_ADMIN, ACCESS_USER_CONTROLLER_OPFR } from '../../enum/enum'


export const Reports = ({ stateForReport, upfr, access, setValueForReport, createReport }) => (
    <div className="journal wrap">
        <FormGroup row className="report">
            <Col sm={12}>
                <h1>Отчеты</h1>
                <hr />
            </Col>
            <Col sm={12}>
                <Formik
                    initialValues={stateForReport}
                    enableReinitialize={true}
                    validationSchema={ReportFormSchema}
                    onSubmit={async (values, { setSubmitting }) => {
                        createReport(values)
                        setSubmitting(false)
                    }}>

                    {({ errors, touched, values, isSubmitting }) => (
                        < Form mode="themed">
                            <Period errors={errors}
                                touched={touched}
                                values={values}
                                setValueForReport={setValueForReport} />
                            <div className="radioBtn">
                                <RadioBtnJournalAndStatisticsTypeTopic setValueForReport={setValueForReport} />
                                <RadioBtnStatisticUsers upfr={upfr} setValueForReport={setValueForReport} />
                                <RadioBtnSecretWordStatisticsAndReportPhone setValueForReport={setValueForReport} />
                                {
                                    access == ACCESS_USER_CONTROLLER_OPFR || access == ACCESS_ADMIN ?
                                        <RadioBtnСomplaint setValueForReport={setValueForReport} />
                                        : null
                                }

                                <div className="btnReport">
                                    <SubmitBtn id="send"
                                        className="btn-success"
                                        type="submit"
                                        disabled={isSubmitting}>
                                        Сформировать отчет
                                    </SubmitBtn>
                                </div>
                            </div>
                        </ Form>
                    )}
                </Formik>

            </Col>
        </FormGroup>
        <ModalServerError />
        {stateForReport.isLoader ? <Loader /> : null}
    </div >
)
