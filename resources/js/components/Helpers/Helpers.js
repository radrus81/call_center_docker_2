
import StringMask from "string-mask"

export function getCreateDate(date) {
    var dt
    if (date != '') {
        let year = date.getFullYear()
        let month = date.getMonth() + 1
        if (month < 10) {
            month = '0' + month
        }
        let day = date.getDate()
        if (day < 10) {
            day = '0' + day
        }
        dt = year + '-' + month + '-' + day
    } else {
        dt = ''
    }
    return dt
}

export function convertObjectToArray(obj) {

    var title = []
    var resultArray = []
    obj.map((tr, index) => {
        var childrenArray = []
        for (var key in tr) {
            if (index == 0) {
                title.push(key)
            }
            var value = tr[key]
            childrenArray.push(value)
        }
        if (index == 0) {
            resultArray.push(title)
        }
        resultArray.push(childrenArray)

    })
    return resultArray
}


export function s2ab(s) {
    var buf = new ArrayBuffer(s.length)
    var view = new Uint8Array(buf)
    for (var i = 0; i < s.length; i++) { view[i] = s.charCodeAt(i) & 0xFF }
    return buf
}

var DELIMITER_SNILS = "-";
const MASK_SNILS = "000-000-000 00";

export function formatValueSnils(str) {

    var unmaskedValue = str.split(DELIMITER_SNILS).join("");
    unmaskedValue = unmaskedValue.replace(' ', '')
    const formatted = StringMask.process(unmaskedValue, MASK_SNILS);
    return removeTrailingCharIfFound(formatted.result, DELIMITER_SNILS);
}

function removeTrailingCharIfFound(str, char) {
    return str
        .split(char)
        .filter(segment => segment !== char)
        .join(char);
}