import React, { useEffect } from "react";
import { Formik } from "formik";
import { Form } from "react-formik-ui";
import { FormGroup, Col } from "reactstrap";
import "../AddCall/AddCall.scss";
import { Snils } from "../AddCall/Fields/Snils";
import CallFormSchema from "../AddCall/CallFormSchema";
import ModalServerError from "../../containers/ModalServerError";

const SecretWord = ({
    access,
    isRequestSecretWord,
    addValueField,
    stateFormik,
    getSecretWord,
    dataSecretWord
}) => {
    return (
        <div className="wrap addCall">
            <FormGroup row>
                <Col sm={7}>
                    <h2>Запрос кодового слова</h2>
                    <hr />
                    <Formik
                        initialValues={stateFormik}
                        enableReinitialize={true}
                        validationSchema={CallFormSchema}
                        onSubmit={async (values, { setSubmitting }) => {
                            addCall(values);
                            setSubmitting(false);
                        }}
                    >
                        {({ errors, values }) => (
                            <Form>
                                <Snils
                                    access={access}
                                    isRequestSecretWord={isRequestSecretWord}
                                    dataSecretWord={dataSecretWord}
                                    errors={errors}
                                    snils={values.snils}
                                    addValueField={addValueField}
                                    getSecretWord={getSecretWord}
                                />
                            </Form>
                        )}
                    </Formik>
                </Col>
            </FormGroup>
            <ModalServerError />
        </div>
    );
};

export default SecretWord;
