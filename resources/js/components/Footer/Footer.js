import React from 'react'
import './Footer.scss'

export const Footer = () => (
    <footer>
        <nav role="navigation" className="navbar fixed-bottom bg-dark">
            <p className="text-white">&copy;&nbsp;ОПФР по Ульяновской области</p>
        </nav>
    </footer>
)