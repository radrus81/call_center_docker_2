import React from "react";
import Actionicons from "../../containers/Actionicons";
import { IconsStatus } from "./IconsStatus";
import TableFilter from "../../containers/TableFilter";

const statusStyle = [
    "table-danger",
    "table-success",
    "table-primary",
    "table-secondary",
    "table-warning"
];

export const Tablesrow = ({ isFilter, calls, message, ChangeStatusCall }) => {
    return (
        <>
            {isFilter ? <TableFilter /> : null}
            {message ? (
                <tr className={statusStyle[0]}>
                    <td colSpan="12">{message}</td>
                </tr>
            ) : (
                calls.map((call, index) => {
                    return (
                        <tr
                            key={call.id + " " + index}
                            className={statusStyle[call.status]}
                        >
                            <td>
                                <Actionicons idObjectCall={index} />
                            </td>
                            <td>{call.codeUpfr}</td>
                            <td>{call.dateStart}</td>
                            <td>
                                {call.familyClient +
                                    " " +
                                    call.nameClient +
                                    " " +
                                    call.middleNameClient}
                            </td>
                            <td>{call.phoneClient}</td>
                            <td>{call.typeOfCounterparty}</td>
                            <td>{call.subtopic}</td>
                            <td>{call.notice}</td>
                            <td>{call.nameUser}</td>
                            <td>
                                <IconsStatus
                                    idObjectCall={index}
                                    idCall={call.idCall}
                                    idStatus={call.status}
                                    ChangeStatusCall={ChangeStatusCall}
                                />
                            </td>
                        </tr>
                    );
                })
            )}
        </>
    );
};
