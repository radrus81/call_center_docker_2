import React, { useEffect } from 'react'
import './journal.scss'
import { Journaltable } from './Journaltable'
import { Pagination } from './Pagination'
import Modaldetailcall from '../../containers/Modaldetailcall'
import ModalDeleteCall from '../../containers/ModalDeleteCall'
import ModalServerError from '../../containers/ModalServerError'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSpinner } from '@fortawesome/free-solid-svg-icons'

const PAGE_SIZE = 30

export const Journal = ({ isFilter, isLoader, calls, message, activePage,
    pageSize, totalCountCalls, GetCalls,
    PageChangeHandler, totalCountCallsForCurrentDate, ChangeStatusCall, setFilter }) => {

    useEffect(() => {
        GetCalls()
    }, [])

    return (
        <>
            <div className="journal wrap">
                <h1>Журнал звонков</h1>
                <div className="pagintioninfo">
                    <div className="spiner">
                        {
                            totalCountCalls > PAGE_SIZE ?
                                <Pagination
                                    pageSize={pageSize}
                                    activePage={activePage}
                                    totalCountCalls={totalCountCalls}
                                    PageChangeHandler={PageChangeHandler}
                                /> : null
                        }
                        {
                            isLoader ? <FontAwesomeIcon className="mr-1 fa-spin mb-3 ml-3 fa-lg" icon={faSpinner} />
                                : null
                        }
                    </div>
                    <p> Сегодня принято&nbsp;<span>{totalCountCallsForCurrentDate}</span>&nbsp;звонков</p>
                    <p> Количество записей на листе&nbsp;<span>{calls.length}</span>&nbsp;из&nbsp;<span>{totalCountCalls}</span></p>
                </div>
                <Journaltable
                    isFilter={isFilter}
                    isLoader={isLoader}
                    calls={calls}
                    message={message}
                    ChangeStatusCall={ChangeStatusCall}
                    setFilter={setFilter}
                />
                <div className="spiner">
                    {
                        totalCountCalls > PAGE_SIZE ?
                            <Pagination
                                pageSize={pageSize}
                                activePage={activePage}
                                totalCountCalls={totalCountCalls}
                                PageChangeHandler={PageChangeHandler}
                            /> : null
                    }
                    {
                        isLoader ? <FontAwesomeIcon className="mr-1 fa-spin mb-3 ml-3 fa-lg" icon={faSpinner} />
                            : null
                    }
                </div>
            </div>
            <Modaldetailcall />
            <ModalDeleteCall />
            <ModalServerError />
        </>
    )
}
