import React from 'react'
import { Tablesrow } from './Tablesrow'
import { TableHeader } from './TableHeader'

export const Journaltable = ({ isFilter, calls, message, ChangeStatusCall, setFilter }) => {

    return (
        <table className="table">
            <thead className="thead-dark">
                <TableHeader
                    isFilter={isFilter}
                    setFilter={setFilter} />
            </thead>
            <tbody>
                <Tablesrow
                    isFilter={isFilter}
                    calls={calls}
                    message={message}
                    ChangeStatusCall={ChangeStatusCall}
                />
            </tbody>
        </table>
    )
}
