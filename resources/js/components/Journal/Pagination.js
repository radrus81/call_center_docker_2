import React from 'react'
import ReactPagination from 'react-js-pagination'

export const Pagination = ({ pageSize, activePage, totalCountCalls, PageChangeHandler }) => {
    return (
        <ReactPagination
            prevPageText={'<'}
            nextPageText={'>'}
            itemsCountPerPage={pageSize}
            activePage={activePage}
            totalItemsCount={totalCountCalls}
            pageRangeDisplayed={6}
            activeClass={'active'}
            itemClass={'page-item'}
            linkClass={'page-link'}
            onChange={PageChangeHandler}
        />
    )
}
