import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch, faTrash } from '@fortawesome/free-solid-svg-icons'
import { ACCESS_USER_OPFR } from '../../enum/enum'

export const Actionicons = ({ access, idObjectCall, ShowDetailCall, ShowModalDeleteCall }) => {
    return (
        <div className="actionTr">
            <FontAwesomeIcon
                title="Посмотреть и редактировать запись"
                icon={faSearch}
                size="lg"
                className="iconFaSearch"
                onClick={() => { ShowDetailCall(idObjectCall) }}
            />
            {access !== ACCESS_USER_OPFR ?
                <FontAwesomeIcon
                    title="Удалить запись"
                    icon={faTrash}
                    size="lg"
                    className="iconFaTrash"
                    onClick={() => { ShowModalDeleteCall(idObjectCall) }}
                />
                : null}
        </div>
    )
}
