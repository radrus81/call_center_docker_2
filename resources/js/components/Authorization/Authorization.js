import React from 'react'
import { Formik, Form, Field } from 'formik'
import AuthorizationFormSchema from './AuthorizationFormSchema'
import './Authorization.scss'
import icon from '../../../img/login.png'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faUser, faKey } from '@fortawesome/free-solid-svg-icons'
import ModalServerError from '../../containers/ModalServerError'

const Authorization = ({ errorMessage, CheckLoginAndPassword }) => {

    return (
        <section className="Auth">
            <div className="Authorization wrap">
                <img src={icon} alt="Scanfcode" />
                <h1>Введите свои данные</h1>
                <Formik
                    initialValues={{
                        login: '',
                        pass: ''
                    }}
                    validationSchema={AuthorizationFormSchema}
                    onSubmit={async (values, { setSubmitting }) => {
                        CheckLoginAndPassword(values.login, values.pass)
                        setSubmitting(false)
                    }}>
                    {({ errors, touched, values, isSubmitting }) => (
                        < Form >
                            {
                                errors.login && touched.login &&
                                <div className="messErr alert-danger">{errors.login}</div>
                            }
                            {
                                errorMessage ?
                                    <div className="messErr alert-danger" dangerouslySetInnerHTML={{ __html: errorMessage }}></div>
                                    : null
                            }
                            <div className="iconInput">
                                <FontAwesomeIcon icon={faUser} size="lg" />
                                <div className="eye">
                                    <Field
                                        name="login"
                                        placeholder="Введите логин"
                                        type="text"
                                        value={values.login || ''}
                                    />
                                </div>
                            </div>
                            {
                                errors.pass && touched.pass &&
                                <div className="messErr alert-danger">{errors.pass}</div>
                            }
                            <div className="iconInput">
                                <FontAwesomeIcon icon={faKey} size="lg" />
                                <div className="eye">
                                    <Field
                                        name="pass"
                                        placeholder="Введите пароль"
                                        type="password"
                                        value={values.pass || ''}
                                    />
                                </div>
                            </div>
                            <button id="send" className="btn-primary" type="submit" disabled={isSubmitting}>Войти</button>
                        </Form>
                    )}
                </Formik>
                <ModalServerError />
            </div>
        </section>
    )
}


export default Authorization
