import React from 'react'
import { FormGroup, Col, Label } from 'reactstrap'
import { SelectComponent } from './SelectComponent'
import DatePicker, { registerLocale } from "react-datepicker"
import "react-datepicker/dist/react-datepicker.css"
import ru from "date-fns/locale/ru"; // the locale you want
registerLocale("ru", ru);
import { TextArea } from './TextArea'


export const Сomplaint = ({ upfr, values, addValueField }) => {
    return (
        <>
            <div className="inputAddCall">
                <SelectComponent nameComponent="complaintsToUpfr" labelName="Жалоба на" data={upfr} addValueField={addValueField} />
                <div className="inputAddCall">
                    <FormGroup row>
                        <Label for="exampleEmail" sm={3}>Дата обращения</Label>
                        <Col sm={9}>
                            <DatePicker
                                locale="ru"
                                selected={values.dateOfIssue}
                                onChange={date => addValueField("dateOfIssue", date)}
                                placeholderText="Дата звонка"
                                dateFormat="dd.MM.yyyy"
                                className="form-control"
                            />
                        </Col>
                    </FormGroup>
                </div>
                <TextArea nameComponent="complaintsDescription"
                    labelName="Описание ситуации"
                    placeholder="Опишите суть проблемы, укажите ФИО специалиста КС"
                    values={values.complaintsDescription}
                    addValueField={addValueField} />
            </div>
        </>
    )
}
