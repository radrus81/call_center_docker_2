import React from 'react'
import { Field } from 'formik'
import { FormGroup, Col, Label } from 'reactstrap'


export const Topics = ({ errors, touched, data, addValueField }) => {

    return (
        <>
            <div className="inputAddCall">
                <FormGroup row>
                    <Label for="exampleEmail" sm={3}>* Тема обращения</Label>
                    <Col sm={9}>
                        <Field
                            component="select"
                            name="topic"
                            className={errors.topic && touched.topic ?
                                "form-control errorValidation"
                                : "form-control"
                            }
                            onChange={(value) => addValueField('topic', parseInt(value.target.value))}>
                            <option value="0">---</option>
                            {
                                data.map((option, index) => {
                                    return (
                                        <option
                                            key={index}
                                            value={option.id}>
                                            {option.name}
                                        </option>
                                    )
                                })
                            }
                        </Field>
                    </Col>
                </FormGroup>
            </div>
        </>
    )
}
