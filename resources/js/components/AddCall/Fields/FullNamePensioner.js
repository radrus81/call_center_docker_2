import React from 'react'
import { Field } from 'formik'
import { Button } from 'react-formik-ui'
import { FormGroup, Col, Label } from 'reactstrap'


export const FullNamePensioner = ({ errors, touched, fullNamePensioner, addValueField, showModalHistoryCalls }) => {

    return (
        <>
            < div className="inputAddCall" >
                <FormGroup row>
                    <Label for="exampleEmail" sm={3}> Ф.И.О</Label>
                    <Col sm={7}>
                        <Field
                            name="fullNamePensioner"
                            placeholder="Введите фамилию имя отчество через пробел"
                            type="text"
                            className="form-control"
                            /*className={errors.fullNamePensioner && touched.fullNamePensioner ?
                                "form-control errorValidation"
                                : "form-control"
                            }*/
                            value={fullNamePensioner || ''}
                            onBlur={event =>
                                addValueField('fullNamePensioner', event.target.value)
                            }
                        />
                    </Col>
                    <div>
                        <Button className="btn-success"
                            onClick={() => showModalHistoryCalls()}>История</Button>
                    </div>
                </FormGroup>
            </div >
        </>
    )
}
