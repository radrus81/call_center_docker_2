import React from 'react'
import { Field } from 'formik'
import { FormGroup, Col, Label } from 'reactstrap'


export const SubTopics = ({ errors, touched, idTopic, data, addValueField }) => {

    return (
        <>
            <div className="inputAddCall">
                <FormGroup row>
                    <Label for="subTopic" sm={3}>* Подтема обращения</Label>
                    <Col sm={9}>
                        <Field component="select" name="subTopic"
                            className={errors.subTopic && touched.subTopic ?
                                "form-control errorValidation"
                                : "form-control"
                            }
                            onChange={(value) => addValueField('subTopic', parseInt(value.target.value))}>>
                            <option value="0">---</option>
                            {

                                data.filter((props) => idTopic === props.idtopic).map((option, index) => {
                                    return (
                                        <option
                                            key={index}
                                            value={option.id}>
                                            {option.name}
                                        </option>
                                    )
                                })
                            }
                        </Field>
                    </Col>
                </FormGroup>
            </div>
        </>
    )
}
