import React from "react";
import { Field } from "formik";
import { FormGroup, Col, Label } from "reactstrap";
import MaskedInput from "react-text-mask";

const phoneNumberMask = [
    "+",
    "7",
    "(",
    /[0-9]/,
    /\d/,
    /\d/,
    ")",
    " ",
    /\d/,
    /\d/,
    /\d/,
    "-",
    /\d/,
    /\d/,
    /\d/,
    /\d/
];

export const Phone = ({ phone, addValueField }) => {
    return (
        <>
            <div className="inputAddCall">
                <FormGroup row>
                    <Label for="exampleEmail" sm={3}>
                        Номер телефона
                    </Label>
                    <Col sm={9}>
                        <Field
                            name="phone"
                            render={({ field }) => (
                                <MaskedInput
                                    {...field}
                                    mask={phoneNumberMask}
                                    placeholder="Введите номер телефона"
                                    type="text"
                                    className="form-control"
                                    value={phone || ""}
                                    onBlur={event =>
                                        addValueField(
                                            "phone",
                                            event.target.value
                                        )
                                    }
                                />
                            )}
                        />
                    </Col>
                </FormGroup>
            </div>
        </>
    );
};
