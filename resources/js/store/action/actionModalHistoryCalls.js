import { SHOW_MODAL_HISTORY_CALLS, CLOSE_MODAL_HISTORY_CALLS, COPY_CALL_FOR_NEW_RECORD } from './actionTypes'
import { ShowModalErrorServer } from './actionModalServerError'


export const showModalHistoryCalls = (page, fullNamePensioner) => {

    return async dispatch => {
        try {
            let errors
            if (!fullNamePensioner) {
                errors = 'Введите данные для поиска истории...'
                let page = 1
                let dataHistoryCalls = []
                let totalCount = 0
                dispatch(historyCalls(page, dataHistoryCalls, totalCount, errors))
            } else {
                errors = ''
                let res = await axios.get('/api/historycalls/' + page + '/' + fullNamePensioner)
                dispatch(historyCalls(page, res.data.dataHistoryCalls, res.data.totalCount, errors))
            }
        }
        catch (error) {
            dispatch(ShowModalErrorServer(error.response.status))
        }
    }
}


export const copyCall = (dataCall) => {
    return {
        type: COPY_CALL_FOR_NEW_RECORD,
        dataCall
    }
}


export const historyCalls = (page, dataHistoryCalls, totalCount, errors) => {

    return {
        type: SHOW_MODAL_HISTORY_CALLS,
        page, dataHistoryCalls, totalCount, errors
    }
}
export const closeModalHistoryCalls = () => {
    return {
        type: CLOSE_MODAL_HISTORY_CALLS
    }
}


