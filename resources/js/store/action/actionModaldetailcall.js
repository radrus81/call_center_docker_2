import {
    SHOW_DETAIL_CALL, CLOSE_DETAIL_CALL,
    SELECT_COMPONENT_FOR_EDIT, LOSS_FOCUS, UPDATE_STORE_AFTER_EDIT
} from './actionTypes'
import { ShowModalErrorServer } from './actionModalServerError'


export function ShowDetailCall(idObjectCall, call) {
    return {
        type: SHOW_DETAIL_CALL,
        idObjectCall, call
    }
}

export function showEditField(tableCallsCellkey) {
    return {
        type: SELECT_COMPONENT_FOR_EDIT,
        tableCallsCellkey
    }
}

export function lossOfFocus() {
    return {
        type: LOSS_FOCUS
    }
}

export function saveEditedCall(idObjectCall, dataCallOriginal) {
    return async dispatch => {
        var newDataFieldsCallForStore = new Map()
        var countFieldForEdit = 0
        var data = new FormData()

        for (var key in dataCallOriginal) {
            if (localStorage.getItem(key)) {
                data.append(key, localStorage.getItem(key))
                newDataFieldsCallForStore.set(key, localStorage.getItem(key))
                countFieldForEdit++
                localStorage.removeItem(key)
            }
        }
        if (countFieldForEdit) {
            data.append('idCall', dataCallOriginal.idCall)
            data.append('_method', 'patch')
            try {
                await axios.post('/api/calls/updatecall', data)
                dispatch(updateReduxStore(idObjectCall, newDataFieldsCallForStore))
                dispatch(closeDetailCall())
            }
            catch (error) {
                dispatch(ShowModalErrorServer(error.response.status))
            }
        } else {
            dispatch(closeDetailCall())
        }
    }
}


export function closeDetailCall() {
    removeItemFromlocalStorage()
    return {
        type: CLOSE_DETAIL_CALL
    }
}

function updateReduxStore(idObjectCall, newDataFieldsCallForStore) {
    return {
        type: UPDATE_STORE_AFTER_EDIT,
        idObjectCall, newDataFieldsCallForStore
    }
}

function removeItemFromlocalStorage() {
    localStorage.removeItem('familyClient')
    localStorage.removeItem('nameClient')
    localStorage.removeItem('middleNameClient')
    localStorage.removeItem('snils')
    localStorage.removeItem('phoneClient')
    localStorage.removeItem('areaOfDislocation')
    localStorage.removeItem('typeOfCounterparty')
    localStorage.removeItem('nameTopicOld')
    localStorage.removeItem('topic')
    localStorage.removeItem('subtopic')
    localStorage.removeItem('typeConsultation')
    localStorage.removeItem('resultConsultation')
    localStorage.removeItem('complaintCodeUpfr')
    localStorage.removeItem('notice')
    localStorage.removeItem('complaintCodeUpfr')
    localStorage.removeItem('dateOfIssue')
    localStorage.removeItem('complaintDescription')
}
