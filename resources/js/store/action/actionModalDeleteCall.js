import { SHOW_MODAL_DELETE_CALL, CLOSE_MODAL_DELETE_CALL, DELETE_CALL } from './actionTypes'
import { ShowModalErrorServer } from './actionModalServerError'

export function ShowModalDeleteCall(idObjectCall, dataCall) {
    return {
        type: SHOW_MODAL_DELETE_CALL,
        idObjectCall, dataCall
    }
}

export function deleteCall(idObjectCall, idCall) {
    return async dispatch => {
        var data = new FormData();
        data.append('id', idCall)
        data.append('_method', 'delete')
        try {
            await axios.post('/api/calls/', data)
            dispatch(deleteSuccess(idObjectCall))
            dispatch(CloseModalDeleteCall())
        }
        catch (error) {
            dispatch(ShowModalErrorServer(error.response.status))
        }
    }

}


function deleteSuccess(idObjectCall) {
    return {
        type: DELETE_CALL,
        idObjectCall
    }
}

export function CloseModalDeleteCall() {
    return {
        type: CLOSE_MODAL_DELETE_CALL
    }
}
