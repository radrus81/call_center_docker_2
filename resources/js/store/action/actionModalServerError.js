import { SHOW_MODAL_SERVER_ERROR, CLOSE_MODAL_SERVER_ERROR } from './actionTypes'


export function ShowModalErrorServer(codeError) {
    return {
        type: SHOW_MODAL_SERVER_ERROR,
        codeError
    }
}

export function CloseModalServerError() {
    return {
        type: CLOSE_MODAL_SERVER_ERROR
    }
}


