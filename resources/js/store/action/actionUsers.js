import { LIST_UPFR_SUCCESS, UPDATE_USERS_LIST, SHOW_ERROR, ADD_USER_IN_STORE } from './actionTypes'
import axios from 'axios'


export function getUsersList(login, fio, kodUpfr) {

    return async dispatch => {
        try {
            let response = await axios.get(`api/users?login=${login}&fio=${fio}&kodUpfr=${kodUpfr}`);
            var data = response.data;
            dispatch(usersListSuccess(null, data.usersList, data.successMessage))
        }
        catch (error) {
            let errorMessage = getServerErrorByCode(error)
            dispatch(showError(errorMessage))
        }
    }
}


export function addUser(login, pass, fio, kodUpfr, access) {
    return async dispatch => {
        var dataForm = new FormData();
        dataForm.append('login', login)
        dataForm.append('password', pass)
        dataForm.append('fio', fio)
        dataForm.append('kod_upfr', kodUpfr)
        dataForm.append('access', access)
        dataForm.append('isBlocked', isBlocked === true || isBlocked === 1 ? 1 : 0)

        try {
            let response = await axios.post('/api/user', dataForm);
            var data = response.data;
            dispatch(addUserSuccess(data.newDataUser))
        }
        catch (error) {
            let errorMessage = getServerErrorByCode(error)
            dispatch(showError(errorMessage))
        }
    }

    function addUserSuccess(newDataUser) {

        return {
            type: ADD_USER_IN_STORE,
            newDataUser,
            successMessage: 'Регистрация прошла успешно'
        }
    }

}

export function getUpfrList() {

    return async dispatch => {
        try {
            let response = await axios.get('api/users/upfrlist')
            var data = response.data;
            dispatch(upfrListAccessSuccess(data.upfrList, data.listAccess, data.successMessage))
        }
        catch (error) {
            let errorMessage = getServerErrorByCode(error)
            dispatch(showError(errorMessage))
        }
    }
}


export function updateUser(id_obj, id, login, password, userFullName, kodUpfr, access, isBlocked) {
    return async dispatch => {

        var data = new FormData();
        data.append('id', id)
        data.append('login', login)
        data.append('pass', password)
        data.append('fio', userFullName)
        data.append('kod_upfr', kodUpfr)
        data.append('access', access)
        data.append('isBlocked', isBlocked === true || isBlocked === 1 ? 1 : 0)
        data.append('_method', 'put')

        try {
            let response = await axios.post('/api/user', data)
            var data = response.data;
            dispatch(usersListSuccess(id_obj, data.newDataUser, data.successMessage))
        }
        catch (error) {
            let errorMessage = getServerErrorByCode(error)
            dispatch(showError(errorMessage))
        }
    }
}



export function getServerErrorByCode(error) {
    if (error.response) {
        var errorMessage = '';
        switch (error.response.status) {
            case 422: errorMessage = 'Указанный логин уже зарегистрован в базе'; break
            case 404: errorMessage = 'Неправильное обращение к серверу.Сообщите администратору'; break
            case 500: errorMessage = 'Произшла ошибка на сервере. Сообщите администратору'; break
            default: errorMessage = 'Не опознанная ошибка. Сообщите администратору. Код=' + error.response.status
        }
        return errorMessage
    }
}


function upfrListAccessSuccess(upfrList, listAccess, successMessage) {

    return {
        type: LIST_UPFR_SUCCESS,
        upfrList, listAccess,
        successMessage
    }
}

function usersListSuccess(id_obj, newDataUser, successMessage) {

    return {
        type: UPDATE_USERS_LIST,
        id_obj, newDataUser, successMessage
    }
}


function showError(errorMessage) {

    return {
        type: SHOW_ERROR,
        errorMessage
    }
}
