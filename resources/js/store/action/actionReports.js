import { ADD_VALUE_FIELD_FOR_REPORT, SHOW_LOADER_FOR_REPORT, HIDE_LOADER_FOR_REPORT } from './actionTypes'
import { getCreateDate } from '../../components/Helpers/Helpers'
import { ShowModalErrorServer } from './actionModalServerError'
import { createExcel, createExcelForReportSubjectOfRequests } from '../../components/Helpers/ServiceExcel'

export const setValueForReport = (fieldName, value, codeUpfr) => {
    return {
        type: ADD_VALUE_FIELD_FOR_REPORT,
        fieldName, value, codeUpfr
    }
}

export const createReport = (values) => {
    return async dispatch => {
        var dataUrl = (getDataForReport(values))

        try {
            dispatch(showLoader())
            let res = await axios.get('/api/reports/' + dataUrl)
            var dataForExcel = getDataForExcel(values, res.data.dataForExcel)
            var typeReport = values.typeReport
            if (typeReport !== 'reportSubjectOfRequests') {
                createExcel(dataForExcel)
            } else {
                createExcelForReportSubjectOfRequests(dataForExcel)
            }
        }
        catch (error) {
            dispatch(ShowModalErrorServer(error.response.status))
        }
        setTimeout(() => { dispatch(hideLoader()) }, 2000)
    }
}

function showLoader() {
    return {
        type: SHOW_LOADER_FOR_REPORT
    }
}

function hideLoader() {

    return {
        type: HIDE_LOADER_FOR_REPORT
    }
}

const getDataForExcel = (values, data) => {

    var dateFrom = getCreateDate(values.dateFrom)
    var dateTo = getCreateDate(values.dateTo)
    var worksheetName = ''

    switch (values.typeReport) {
        case 'printJournal': worksheetName = 'Журнал принятых звонков'; break
        case 'oldStatistic': worksheetName = 'Статистика по старым темам'; break
        case 'newStatistic': worksheetName = 'Статистика по новым темам'; break
        case 'statisticUsers': worksheetName = 'Статистика по пользователям'; break
        case 'secretWordStatistic': worksheetName = 'Статистика по кодовому слову'; break
        case 'reportJournalPhone': worksheetName = 'Журнал учета телефонных обращений граждан'; break
        case 'reportSubjectOfRequests': worksheetName = 'Информация об организации дистанционного обслуживания граждан'; break
        case 'complaint':
            if (values.reportСomplaint === 'journalСomplaint') {
                worksheetName = 'Жалобы на УПФР'
            } else {
                worksheetName = 'Статистика жалоб на УПФР'
            }; break
        default: worksheetName = 'Журнал'
    }
    var dataForExcel = []
    dataForExcel.push(worksheetName + '.xlsx',
        {
            worksheetName: worksheetName,
            title: worksheetName + ' с ' + dateFrom + ' по ' + dateTo,
            data: data
        })
    return dataForExcel
}

const getDataForReport = (values) => {

    var dateFrom = getCreateDate(values.dateFrom)
    var dateTo = getCreateDate(values.dateTo)
    var typeReport = values.typeReport
    var codeUpfrForReport = 0
    var reportRadioTypeTopic = ''
    var reportСomplaint = ''

    switch (values.typeReport) {
        case 'statisticUsers': if (!values.codeUpfr) {
            return false
        } else { codeUpfrForReport = values.codeUpfr };
            reportRadioTypeTopic = values.reportRadioTypeTopic; break
        case 'complaint': reportСomplaint = values.reportСomplaint; break
    }


    return `?dateFrom=${dateFrom}&dateTo=${dateTo}&typeReport=${typeReport}
    &codeUpfrForReport=${codeUpfrForReport}&reportRadioTypeTopic=${reportRadioTypeTopic}
    &reportСomplaint=${reportСomplaint}&access=${localStorage.getItem('access')}&codeUpfr=${localStorage.getItem('kod_upfr')}`
}
