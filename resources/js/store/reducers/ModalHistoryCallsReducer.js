import { SHOW_MODAL_HISTORY_CALLS, CLOSE_MODAL_HISTORY_CALLS, COPY_CALL_FOR_NEW_RECORD } from '../action/actionTypes'

const initialState = {
    isModalOpen: false,
    isModalClose: true,
    dataHistoryCalls: [],
    totalCount: 0,
    activePage: 1,
    errors: ''
}

export default function ModalHistoryCallsReducer(state = initialState, action) {

    switch (action.type) {

        case SHOW_MODAL_HISTORY_CALLS:
            return {
                ...state,
                isModalOpen: true,
                isModalClose: false,
                activePage: action.page,
                dataHistoryCalls: action.dataHistoryCalls,
                totalCount: action.totalCount,
                errors: action.errors

            }
        case CLOSE_MODAL_HISTORY_CALLS:
        case COPY_CALL_FOR_NEW_RECORD:
            return initialState


        default:
            return state
    }
}
