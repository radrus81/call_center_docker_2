import {
    SHOW_LOADER, CALLS_SUCCESS, CALLS_EMPTY,
    UPDATE_STORE_AFTER_EDIT, DELETE_CALL, CHANGE_STATUS_CALL_SUCCESS,
    SET_USERS_LAST_CALLS_AND_STATISTIC, CHANGE_STATUS_CALL_SUCCESS_FOR_USER
} from '../action/actionTypes'

const PAGE_SIZE = 30
const initialState = {
    isLoader: true,
    calls: [],
    message: '',
    pageSize: PAGE_SIZE,
    activePage: 1,
    totalCountCalls: 0,
    totalCountCallsForCurrentDate: 0,
    usersCalls: [],
    userStatistic: []
}

export default function JournalReducer(state = initialState, action) {

    switch (action.type) {
        case SHOW_LOADER:
            return {
                ...state,
                isLoader: true
            }
        case CALLS_SUCCESS:
            return {
                ...state,
                calls: action.calls,
                totalCountCalls: action.totalCountCalls,
                message: '',
                activePage: action.activePage,
                isLoader: false,
                totalCountCallsForCurrentDate: action.totalCountCallsForCurrentDate
            }
        case UPDATE_STORE_AFTER_EDIT:
            let newdataCalls = _.cloneDeep(state.calls)
            for (let enter of action.newDataFieldsCallForStore) {
                newdataCalls[action.idObjectCall][enter[0]] = enter[1]
            }
            return {
                ...state,
                calls: newdataCalls
            }
        case DELETE_CALL:
            let newCalls = _.cloneDeep(state.calls)
            newCalls.splice(action.idObjectCall, 1)
            return {
                ...state,
                calls: newCalls
            }

        case CHANGE_STATUS_CALL_SUCCESS:
            let newCallsForChange = _.cloneDeep(state.calls)
            newCallsForChange[action.idObjectCall].status = action.newStatus
            return {
                ...state,
                calls: newCallsForChange
            }
        case CHANGE_STATUS_CALL_SUCCESS_FOR_USER:
            let newCallsForChangeUser = _.cloneDeep(state.usersCalls)
            newCallsForChangeUser[action.idObjectCall].status = action.newStatus
            return {
                ...state,
                usersCalls: newCallsForChangeUser,
                userStatistic: action.userStatistic
            }

        case CALLS_EMPTY:
            return {
                ...state,
                calls: [],
                message: 'Нет данных',
                isLoader: false,
                totalCountCalls: action.totalCountCalls
            }
        case SET_USERS_LAST_CALLS_AND_STATISTIC:
            return {
                ...state,
                usersCalls: action.usersCalls,
                userStatistic: action.userStatistic
            }

        default:
            return state
    }
}
