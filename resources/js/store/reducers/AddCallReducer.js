import {
    ADD_VALUE_FIELD_FOR_CALL, INSERT_CALL_SUCCESS,
    SHOW_TABLE_SECRET_WORD, DATA_SECRET_WORD, SHOW_ERROR_SERVER_TO_TABLE,
    COPY_CALL_FOR_NEW_RECORD
} from '../action/actionTypes'



const initialState = {
    timeStart: 0,
    fullNamePensioner: '',
    snils: '',
    isRequestSecretWord: false,
    dataSecretWord: [],
    codeWord: 0,
    phone: '',
    areaOfDislocation: 0,
    typeCounterparty: 0,
    topic: 0,
    subTopic: 0,
    typeConsultation: 0,
    resultConsultation: 0,
    notice: '',
    isСomplaint: false,
    complaintsToUpfr: 83,
    dateOfIssue: '',
    complaintsDescription: '',
    status: 1,
    message: '',
    countRecord:1
}


export default function AddCallReducer(state = initialState, action) {

    switch (action.type) {

        case ADD_VALUE_FIELD_FOR_CALL:
            return {
                ...state,
                timeStart: action.fieldName === 'fullNamePensioner' && !state.timeStart ?
                    parseInt((new Date().getTime() + (new Date().getTimezoneOffset() * 60)) / 1000) : state.timeStart,
                fullNamePensioner: action.fieldName === 'fullNamePensioner' ? action.value : state.fullNamePensioner,
                snils: action.fieldName === 'snils' ? action.value : state.snils,
                codeWord: action.fieldName === 'codeWord' ? action.value : state.codeWord,
                phone: action.fieldName === 'phone' ? action.value : state.phone,
                areaOfDislocation: action.fieldName === 'areaOfDislocation' ? action.value : state.areaOfDislocation,
                typeCounterparty: action.fieldName === 'typeCounterparty' ? action.value : state.typeCounterparty,
                topic: action.fieldName === 'topic' ? action.value : state.topic,
                subTopic: action.fieldName === 'subTopic' ? action.value : state.subTopic,
                typeConsultation: action.fieldName === 'typeConsultation' ? action.value : state.typeConsultation,
                resultConsultation: action.fieldName === 'resultConsultation' ? action.value : state.resultConsultation,
                notice: action.fieldName === 'notice' ? action.value : state.notice,
                isСomplaint: action.fieldName === 'isСomplaint' ? action.value : state.isСomplaint,
                complaintsToUpfr: action.fieldName === 'complaintsToUpfr' ? action.value :
                    state.isСomplaint ? state.complaintsToUpfr : 83,
                dateOfIssue: action.fieldName === 'dateOfIssue' ? action.value :
                    state.isСomplaint ? state.dateOfIssue : '',
                complaintsDescription: action.fieldName === 'complaintsDescription' ? action.value :
                    state.isСomplaint ? state.complaintsDescription : '',
                status: action.fieldName === 'status' ? action.value : state.status,
                message: action.fieldName === 'fullNamePensioner' && state.message ? '' : '',
                countRecord: action.fieldName === 'countRecord' ? action.value : state.countRecord,
            }
        case INSERT_CALL_SUCCESS:
            let newState = _.cloneDeep(initialState)
            newState.message = 'Звонок успешно добавлен'
            return newState
        case SHOW_TABLE_SECRET_WORD:
            return {
                ...state,
                dataSecretWord: [],
                isRequestSecretWord: true
            }
        case DATA_SECRET_WORD:
            return {
                ...state,
                dataSecretWord: action.dataSecretWord
            }
        case SHOW_ERROR_SERVER_TO_TABLE:
            return {
                ...state,
                dataSecretWord: [{ 'base': 'ПТК КС', 'createDate': 'Ошибка ', 'question': 'сервера', 'answer': 'код: ' + action.codeError }]
            }
        case COPY_CALL_FOR_NEW_RECORD:
            let cloneState = _.cloneDeep(initialState)
            cloneState.fullNamePensioner = action.dataCall.familyClient + ' ' + action.dataCall.nameClient + ' ' + action.dataCall.middleNameClient
            cloneState.snils = action.dataCall.snils
            cloneState.phone = action.dataCall.phoneClient
            cloneState.areaOfDislocation = action.dataCall.idRaion
            cloneState.typeCounterparty = action.dataCall.idKontragent
            return cloneState

        default:
            return state
    }
}

