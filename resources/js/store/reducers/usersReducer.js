import { LIST_UPFR_SUCCESS, UPDATE_USERS_LIST, SHOW_ERROR, ADD_USER_IN_STORE } from "../action/actionTypes"

const initialState = {
    usersList: [],
    upfrList: [],
    listAccess: [],
    errorMessage: '',
    successMessage: ''
}

export default function UsersReducer(state = initialState, action) {

    switch (action.type) {

        case ADD_USER_IN_STORE:
            var newArrayForUsers = []
            newArrayForUsers.push(action.newDataUser)
            let newUsersListAdd = _.cloneDeep(state.usersList)
            newUsersListAdd.map((newData, index) => {
                newArrayForUsers.push(newData)
            })
            return {
                ...state,
                usersList: newArrayForUsers,
                successMessage: action.successMessage,
                errorMessage: ''
            }

        case UPDATE_USERS_LIST:
            let newUsersList = _.cloneDeep(state.usersList)
            if (action.id_obj !== null) {
                newUsersList[action.id_obj] = action.newDataUser
            } else {
                newUsersList = action.newDataUser
            }
            return {
                ...state,
                usersList: newUsersList,
                errorMessage: '',
                successMessage: ''
            }
        case LIST_UPFR_SUCCESS:
            return {
                ...state,
                upfrList: action.upfrList,
                listAccess: action.listAccess,
                errorMessage: '',
                successMessage: ''
            }

        case SHOW_ERROR:
            return {
                ...state,
                errorMessage: action.errorMessage,
                successMessage: ''
            }

        default:
            return state
    }
}
