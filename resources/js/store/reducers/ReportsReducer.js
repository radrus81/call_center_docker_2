import { ADD_VALUE_FIELD_FOR_REPORT, SHOW_LOADER_FOR_REPORT, HIDE_LOADER_FOR_REPORT } from '../action/actionTypes'

const initialState = {
    dateFrom: '',
    dateTo: '',
    typeReport: 'printJournal',
    codeUpfr: 0,
    reportRadioTypeTopic: 'newTopic',
    reportСomplaint: 'journalСomplaint',
    isLoader: false
}

export default function ReportsReducer(state = initialState, action) {

    switch (action.type) {

        case ADD_VALUE_FIELD_FOR_REPORT:
            return {
                ...state,
                dateFrom: action.fieldName === 'dateFrom' ? action.value : state.dateFrom,
                dateTo: action.fieldName === 'dateTo' ? action.value : state.dateTo,
                typeReport: action.fieldName === 'typeReport' ? action.value : state.typeReport,
                codeUpfr: action.fieldName === 'codeUpfr' ? action.value :
                    action.value === 'statisticUsers' ? action.codeUpfr : state.codeUpfr,
                reportRadioTypeTopic: action.fieldName === 'reportRadioTypeTopic' ? action.value : state.reportRadioTypeTopic,
                reportСomplaint: action.fieldName === 'reportСomplaint' ? action.value : state.reportСomplaint
            }
        case SHOW_LOADER_FOR_REPORT: {
            return {
                ...state,
                isLoader: true
            }
        }
        case HIDE_LOADER_FOR_REPORT: {
            return {
                ...state,
                isLoader: false
            }
        }

        default:
            return state
    }
}
