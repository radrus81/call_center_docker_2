import { combineReducers } from 'redux'
import authorization from './AuthorizationReducer'
import journal from './JournalReducer'
import modaldetailcall from './ModalDetailCallReducer'
import directories from './DirectoriesReducer'
import ModalDeleteCall from './ModalDeleteCallReducer'
import TableFilter from './TableFilterReducer'
import DataForCall from './AddCallReducer'
import ServerError from './ModalServerErrorReducer'
import Reports from './ReportsReducer'
import users from './UsersReducer'
import historyCalls from './ModalHistoryCallsReducer'


export default combineReducers({
    authorization,
    journal,
    modaldetailcall,
    directories,
    ModalDeleteCall,
    TableFilter,
    DataForCall,
    ServerError,
    Reports,
    users,
    historyCalls
})
