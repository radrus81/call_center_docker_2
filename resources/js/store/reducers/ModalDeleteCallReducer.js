import { SHOW_MODAL_DELETE_CALL, CLOSE_MODAL_DELETE_CALL } from '../action/actionTypes'

const initialState = {
    isModalOpen: false,
    isModalClose: true,
    idObjectCall: null,
    dataCall: []
}

export default function ModalDeleteCallReducer(state = initialState, action) {

    switch (action.type) {

        case SHOW_MODAL_DELETE_CALL:
            return {
                ...state,
                isModalOpen: true,
                isModalClose: false,
                idObjectCall: action.idObjectCall,
                dataCall: action.dataCall
            }
        case CLOSE_MODAL_DELETE_CALL:
            return {
                ...state,
                isModalOpen: false,
                isModalClose: true,
                idObjectCall: null,
                dataCall: []
            }
        default:
            return state
    }
}
