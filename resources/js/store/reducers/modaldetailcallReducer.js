import { SHOW_DETAIL_CALL, CLOSE_DETAIL_CALL, SELECT_COMPONENT_FOR_EDIT, LOSS_FOCUS } from '../action/actionTypes'

const initialState = {
    isModalOpen: false,
    isModalClose: true,
    idObjectCall: 0,
    dataCall: [],
    tableCallsCellkey: '',
    allAreaOfResidense: []
}

export default function ModalDetailCallReducer(state = initialState, action) {
    var cloneCall = []
    if (action.call) {
        for (var key in action.call) {
            if (key !== 'statusName' && key !== 'status') {
                cloneCall[key] = action.call[key]
            }
        }
    }

    switch (action.type) {

        case SHOW_DETAIL_CALL:
            return {
                ...state,
                isModalOpen: true,
                isModalClose: false,
                idObjectCall: action.idObjectCall,
                dataCall: cloneCall
            }
        case SELECT_COMPONENT_FOR_EDIT:
            return {
                ...state,
                tableCallsCellkey: action.tableCallsCellkey
            }
        case LOSS_FOCUS:
            return {
                ...state,
                tableCallsCellkey: ''
            }
        case CLOSE_DETAIL_CALL:
            return {
                ...state,
                isModalOpen: false,
                isModalClose: true,
                dataCall: []
            }
        default:
            return state
    }
}
