import { connect } from 'react-redux'
import { ModalServerError } from '../components/Modals/ModalServerError/ModalServerError'
import { CloseModalServerError } from '../store/action/actionModalServerError'



const mapStateToProps = state => ({
    isModalOpen: state.ServerError.isModalOpen,
    isModalClose: state.ServerError.isModalClose,
    codeError: state.ServerError.codeError
})

const mergeProps = (stateProps, dispatchProps, ownProps) => {
    const { } = stateProps
    const { dispatch } = dispatchProps

    return {
        ...stateProps,
        modalClose: () => dispatch(CloseModalServerError())
    }
}

export default connect(mapStateToProps, null, mergeProps)(ModalServerError)
