import { connect } from 'react-redux'
import Authorization from '../components/Authorization/Authorization'
import { CheckLoginAndPassword } from '../store/action/actionAuthorization'


const mapStateToProps = state => ({
    errorMessage: state.authorization.errorMessage
})

const mapDispatchToProps = dispatch => ({
    CheckLoginAndPassword: (login, password) => dispatch(CheckLoginAndPassword(login, password))
})

export default connect(mapStateToProps, mapDispatchToProps)(Authorization)
