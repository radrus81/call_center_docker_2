import { connect } from "react-redux";
import { TableFilter } from "../components/Journal/TableFilter";
import { setValueForFilter, GetCalls } from "../store/action/actionJournal";

const mapStateToProps = state => ({
    //данные для поиска
    isFilter: state.TableFilter.isFilter,
    codeUpfrFromFilter: state.TableFilter.codeUpfr,
    createDateFromFilter: state.TableFilter.createDate,
    fullNameClientFromFilter: state.TableFilter.fullNameClient,
    phoneClientFromFilter: state.TableFilter.phone,
    typeContractorFromFilter: state.TableFilter.typeContractor,
    subTopicFromFilter: state.TableFilter.subTopic,
    noticeFromFilter: state.TableFilter.notice,
    userFromFilter: state.TableFilter.user,
    statusFromFilter: state.TableFilter.status,
    //данные для строки фильтра
    upfr: state.directories.upfr,
    counterparties: state.directories.counterparties,
    subTopics: state.directories.subTopics,
    statuses: state.directories.status,
    users: state.directories.users,
    //допол данные
    codeUpfr: state.authorization.kodUpfr,
    access: state.authorization.access,
    activePage: state.journal.activePage,
    pageSize: state.journal.pageSize
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
    const {
        isFilter,
        codeUpfrFromFilter,
        createDateFromFilter,
        fullNameClientFromFilter,
        phoneClientFromFilter,
        typeContractorFromFilter,
        subTopicFromFilter,
        noticeFromFilter,
        userFromFilter,
        statusFromFilter,
        codeUpfr,
        access,
        activePage,
        pageSize
    } = stateProps;
    const { dispatch } = dispatchProps;

    const args = [
        codeUpfrFromFilter,
        createDateFromFilter,
        fullNameClientFromFilter,
        typeContractorFromFilter,
        subTopicFromFilter,
        noticeFromFilter,
        userFromFilter,
        statusFromFilter,
        codeUpfr,
        access,
        pageSize,
        phoneClientFromFilter
    ];

    return {
        ...stateProps,
        setValueForFilter: (fieldName, value) =>
            dispatch(setValueForFilter(fieldName, value)),
        startFilter: page => dispatch(GetCalls(page, isFilter, ...args))
    };
};

export default connect(mapStateToProps, null, mergeProps)(TableFilter);
