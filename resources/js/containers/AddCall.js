import { connect } from 'react-redux'
import AddCall from '../components/AddCall/AddCall'
import { addValueField, addCall, getUserCallsAndStatistic, getSecretWord } from '../store/action/actionAddCall'
import { ChangeStatusCall } from '../store/action/actionJournal'
import { showModalHistoryCalls } from '../store/action/actionModalHistoryCalls'

const mapStateToProps = state => ({
    access: state.authorization.access,
    stateFormik: state.DataForCall,
    areaOfDislocation: state.directories.areaOfDislocation,
    counterparties: state.directories.counterparties,
    topics: state.directories.topics,
    subTopics: state.directories.subTopics,
    typeConsultation: state.directories.typeConsultation,
    resultConsultation: state.directories.resultConsultation,
    upfr: state.directories.upfr,
    usersCalls: state.journal.usersCalls,
    userStatistic: state.journal.userStatistic,
    message: state.DataForCall.message,
    snils: state.DataForCall.snils,
    isRequestSecretWord: state.DataForCall.isRequestSecretWord,
    dataSecretWord: state.DataForCall.dataSecretWord,
    fullNamePensioner: state.DataForCall.fullNamePensioner
})

const mergeProps = (stateProps, dispatchProps, ownProps) => {
    const { snils, fullNamePensioner } = stateProps
    const { dispatch } = dispatchProps

    return {
        ...stateProps,
        addValueField: (fieldName, value) => dispatch(addValueField(fieldName, value)),
        addCall: (values) => dispatch(addCall(values)),
        getUserCallsAndStatistic: () => dispatch(getUserCallsAndStatistic()),
        ChangeStatusCall: (idObjectCall, idCall, newStatus, isCallOnlyUser = true) =>
            dispatch(ChangeStatusCall(idObjectCall, idCall, newStatus, isCallOnlyUser)),
        getSecretWord: () => dispatch(getSecretWord(snils)),
        showModalHistoryCalls: (page = 1) => dispatch(showModalHistoryCalls(page, fullNamePensioner))
    }
}

export default connect(mapStateToProps, null, mergeProps)(AddCall)
