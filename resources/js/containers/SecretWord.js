import { connect } from "react-redux";
import SecretWord from "../components/SecretWord/SecretWord";
import { addValueField, getSecretWord } from "../store/action/actionAddCall";

const mapStateToProps = state => ({
    access: state.authorization.access,
    snils: state.DataForCall.snils,
    stateFormik: state.DataForCall,
    dataSecretWord: state.DataForCall.dataSecretWord,
    isRequestSecretWord: state.DataForCall.isRequestSecretWord
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
    const { snils } = stateProps;
    const { dispatch } = dispatchProps;

    return {
        ...stateProps,
        addValueField: (fieldName, value) =>
            dispatch(addValueField(fieldName, value)),
        getSecretWord: () => dispatch(getSecretWord(snils))
    };
};

export default connect(mapStateToProps, null, mergeProps)(SecretWord);
