import { connect } from "react-redux";
import { Journal } from "../components/Journal/Journal";
import {
    GetCalls,
    ChangeStatusCall,
    setFilter
} from "../store/action/actionJournal";

const mapStateToProps = state => ({
    isFilter: state.TableFilter.isFilter,
    isLoader: state.journal.isLoader,
    codeUpfr: state.authorization.kodUpfr,
    access: state.authorization.access,
    calls: state.journal.calls,
    message: state.journal.message,
    activePage: state.journal.activePage,
    pageSize: state.journal.pageSize,
    totalCountCalls: state.journal.totalCountCalls,
    totalCountCallsForCurrentDate: state.journal.totalCountCallsForCurrentDate,
    //данные для фильтра
    codeUpfrFromFilter: state.TableFilter.codeUpfr,
    createDateFromFilter: state.TableFilter.createDate,
    fullNameClientFromFilter: state.TableFilter.fullNameClient,
    phoneClientFromFilter: state.TableFilter.phone,
    typeContractorFromFilter: state.TableFilter.typeContractor,
    subTopicFromFilter: state.TableFilter.subTopic,
    noticeFromFilter: state.TableFilter.notice,
    userFromFilter: state.TableFilter.user,
    statusFromFilter: state.TableFilter.status
});

const mergeProps = (stateProps, dispatchProps, ownProps) => {
    const {
        isFilter,
        codeUpfrFromFilter,
        createDateFromFilter,
        fullNameClientFromFilter,
        phoneClientFromFilter,
        typeContractorFromFilter,
        subTopicFromFilter,
        noticeFromFilter,
        userFromFilter,
        statusFromFilter,
        codeUpfr,
        access,
        pageSize
    } = stateProps;
    const { dispatch } = dispatchProps;

    const args = [
        codeUpfrFromFilter,
        createDateFromFilter,
        fullNameClientFromFilter,
        typeContractorFromFilter,
        subTopicFromFilter,
        noticeFromFilter,
        userFromFilter,
        statusFromFilter,
        codeUpfr,
        access,
        pageSize,
        phoneClientFromFilter
    ];

    return {
        ...stateProps,
        setFilter: () =>
            isFilter
                ? dispatch(GetCalls(1, !isFilter, ...args))
                : dispatch(setFilter(!isFilter)),
        GetCalls: (page = 1) => dispatch(GetCalls(page, isFilter, ...args)),
        PageChangeHandler: page => dispatch(GetCalls(page, isFilter, ...args)),
        ChangeStatusCall: (
            idObjectCall,
            idCall,
            newStatus,
            isCallOnlyUser = false
        ) =>
            dispatch(
                ChangeStatusCall(
                    idObjectCall,
                    idCall,
                    newStatus,
                    isCallOnlyUser
                )
            )
    };
};

export default connect(mapStateToProps, null, mergeProps)(Journal);
