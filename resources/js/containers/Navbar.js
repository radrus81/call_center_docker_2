import { connect } from 'react-redux'
import { Navbar } from '../components/Navbar/Navbar'


const mapStateToProps = state => ({
    isAuthorization: state.authorization.isAuthorization,
    access: state.authorization.access
})

export default connect(mapStateToProps, null)(Navbar)
