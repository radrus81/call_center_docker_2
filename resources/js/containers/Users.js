import { connect } from 'react-redux'
import Users from '../components/Users/Users'
import { getUsersList, getUpfrList, addUser, updateUser } from '../store/action/actionUsers'


const mapStateToProps = state => ({//общий state из всех reduceres, описаны в папке state/reducer
    usersList: state.users.usersList,//нужно для того чтобы записать в props значение из store->authorization
    upfrList: state.users.upfrList,
    listAccess: state.users.listAccess,
    errorMessage: state.users.errorMessage,
    successMessage: state.users.successMessage,
    showFieldPasswordEdit: state.authorization.showFieldPasswordEdit
})

const mapDispatchToProps = dispatch => ({
    getUsersList: (login, userFullName, upfrCode) => dispatch(getUsersList(login, userFullName, upfrCode)),
    getUpfrList: () => dispatch(getUpfrList()),
    addUser: (login, pass, userFullName, upfrCode, access, isBlocked) => dispatch(addUser(login, pass, userFullName, upfrCode, access, isBlocked)),
    updateUser: (id_obj, id, login, pass, userFullName, upfrCode, access, isBlocked) => dispatch(updateUser(id_obj, id, login, pass, userFullName, upfrCode, access, isBlocked)),
})

export default connect(mapStateToProps, mapDispatchToProps)(Users)
